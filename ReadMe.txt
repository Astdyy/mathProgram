This program do:
1. Parses mathematical expressions;
2. Checks the correctness of the entered expression;
3. Offers to enter the value of all variables that are present in the expression;
4. Calculates the result of the expression;