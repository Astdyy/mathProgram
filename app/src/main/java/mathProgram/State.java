package mathProgram;

public enum State {
    SEARCH(0), DIGIT_RECORDER(1),ERROR(-1),MATHEMATICAL_SIGN_RECORDER(2);
    private final int code;

    private  State(int code)
    {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}