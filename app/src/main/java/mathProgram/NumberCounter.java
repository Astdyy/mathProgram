package mathProgram;

import java.util.List;

public class NumberCounter {
    private UnknownValuesParser unknownValuesParser = new UnknownValuesParser();
    private Calculator calculator = new Calculator();
    MathematicalExpression mathematicalExpression;
    public int countUnknownValues(String mathEx){
        return unknownValuesParser.fiendUnknownValues(mathEx);
    }
    public String countNumbers(List<Double> numbersByUnknownValues) throws Exception {
        mathematicalExpression =  unknownValuesParser.addUnknownValues(numbersByUnknownValues);
        return calculator.сalculate(mathematicalExpression);
    }
    public  void getHistory()
    {
        Visualization.visualize(calculator.getHistoryCalculate(),mathematicalExpression);
    }

}
