package mathProgram;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    private List<NodeHistory> historyCalculate;

    public Calculator() {
        historyCalculate = new ArrayList<>();
    }

    public List<NodeHistory> getHistoryCalculate() {
        return historyCalculate;
    }

    public String сalculate(MathematicalExpression mathematicalExpression) throws Exception {
        historyCalculate.clear();
        ArrayList<String> reversePrefixNotationList = parseToReversePrefixNotationList(mathematicalExpression);
        ContainerPart[] convertValues = new ContainerPart[]{new ContainerPart(), new ContainerPart()};
        for (int i = 0; i < reversePrefixNotationList.size(); i++) {
            if (reversePrefixNotationList.size() == 1) {
                break;
            } else if (isDigit(reversePrefixNotationList.get(i))) {
                if (convertValues[0].partMathEx.length() == 0) {
                    convertValues[0].setPart(reversePrefixNotationList.get(i), i);
                } else if (convertValues[1].partMathEx.length() == 0) {
                    convertValues[1].setPart(reversePrefixNotationList.get(i), i);
                } else {
                    convertValues[0] = new ContainerPart();
                    convertValues[0].setPart(convertValues[1].partMathEx, convertValues[1].index);
                    convertValues[1].setPart(reversePrefixNotationList.get(i), i);
                }
            } else if (MathematicalSigns.isMathSigns(reversePrefixNotationList.get(i))) {
                if (convertValues[0].partMathEx.length() > 0 & convertValues[1].partMathEx.length() > 0) {
                    String result = countNumbers(convertValues, reversePrefixNotationList.get(i));
                    reversePrefixNotationList.set(convertValues[0].index, result);
                    reversePrefixNotationList.remove(convertValues[1].index);
                    reversePrefixNotationList.remove(i - 1);
                    int ds = 0;
                }
                   for (ContainerPart containerPart : convertValues) {
                       containerPart.clear();
                   }

                i = -1;
            }
        }
        if(reversePrefixNotationList.get(0).contains(".")) {
            String res = reversePrefixNotationList.get(0);
            boolean isMantissaEmpty = true;
                for (int i = res.indexOf(".") +1 ; i < res.length(); i++) {
                if (res.charAt(i) != '0')
                {
                    isMantissaEmpty= false;
                }
            }
            if (isMantissaEmpty) {
                StringBuilder trimResult = new StringBuilder();
                for (int i = 0; i < res.indexOf("."); i++) {
                    trimResult.append(res.charAt(i));
                }
                return trimResult.toString();
            }
        }
        return reversePrefixNotationList.get(0);
    }

    public ArrayList<String> parseToReversePrefixNotationList(MathematicalExpression mathematicalExpression) throws Exception {
        if (mathematicalExpression.getMathExpression() == null)
        {
            throw new IllegalArgumentException();
        }
        List<String> Expression = mathematicalExpression.convertToDecimalSystem();
        Stack<String> sings = new Stack<>();
        ArrayList<String> result = new ArrayList<>();
        for (String partExpression : Expression) {
            if (isDigit(partExpression)) {
                result.add(partExpression);
            } else if (MathematicalSigns.isMathSigns(partExpression)) {
                if (partExpression.equals("+") || partExpression.equals("-")) {
                    if (!sings.contains("*") & !sings.contains("/") & sings.size() != 0 & !sings.contains("(")) {
                        result.add(sings.pop());
                    } else {
                        while (sings.size() > 0) {
                            String temp = sings.pop();
                            if (temp.equals("(")) {
                                sings.push(temp);
                                break;
                            }
                            result.add(temp);
                        }
                    }
                    sings.push((partExpression));
                } else if (partExpression.equals("*") || partExpression.equals("/")) {
                    if (sings.contains("*")) {
                        sings.remove(sings.indexOf("*"));
                        result.add("*");
                    } else if (sings.contains("/")) {
                        sings.remove(sings.indexOf("/"));
                        result.add("/");
                    }
                    sings.push(partExpression);
                }
            } else if (partExpression.equals("(")) {
                sings.push(partExpression);
            } else if (partExpression.equals(")")) {
                Boolean isOpenParenthesis = false;
                do {
                    String temp = sings.pop();
                    if (temp.equals("(")) {
                        isOpenParenthesis = true;
                    } else {
                        result.add(temp);
                    }
                } while (!isOpenParenthesis);
            }
        }

        if (sings.contains("*") || sings.contains("/")) {
            while (sings.size() > 0) {
                result.add(sings.pop());
            }
        } else if (sings.size() != 0) {
            result.addAll(sings);
        }
        return result;
    }

    public String parseToReversePrefixNotation(MathematicalExpression mathematicalExpression) throws Exception {
        List<String> result = parseToReversePrefixNotationList(mathematicalExpression);
        return String.join(" ", result.toArray(String[]::new));
    }

    private boolean isDigit(String str) {
        boolean isStrDigit = true;
        if (str.length() > 1) {
            if (str.charAt(0) == '-' | Character.isDigit(str.charAt(0))) {
                for (int i = 1; i < str.length(); i++) {
                    if (!Character.isDigit(str.charAt(i)) && !MathematicalSigns.isTypeSystemsSigns(str.charAt(i))) {
                        isStrDigit = false;
                    }
                }
            }else{
                isStrDigit = false;
            }
        } else {
            isStrDigit = Character.isDigit(str.charAt(0));
        }
        return isStrDigit;
    }

    private String countNumbers(ContainerPart[] containerParts, String sing) throws MathematicalExpressionError {
        boolean isRealNumbers =false;
        Number firsNumber;
        Number secondNumber;
        try
        {
            if (containerParts[0].getPartMathEx().contains(".") ||containerParts[1].getPartMathEx().contains(".") || sing == "/") {
                isRealNumbers = true;
                firsNumber = Double.parseDouble(containerParts[0].getPartMathEx());
                secondNumber = Double.parseDouble(containerParts[1].getPartMathEx());
            }else {
                firsNumber = Long.parseLong(containerParts[0].getPartMathEx());
                secondNumber = Long.parseLong(containerParts[1].getPartMathEx());
            }
        }catch (Exception e)
        {
            throw  new MathematicalExpressionError("Parse Value Error");
        }
        String result = new String();
        switch (sing) {
            case "+":
                if (isRealNumbers) {
                    double temp1 = firsNumber.doubleValue();
                    double temp2 = secondNumber.doubleValue();
                    result = Double.toString(temp1 + temp2);
                }
                else
                    result = Long.toString(firsNumber.longValue() + secondNumber.longValue());
                break;
            case "-":
                if (isRealNumbers) {
                    double temp1 = firsNumber.doubleValue();
                    double temp2 = secondNumber.doubleValue();
                    result = Double.toString(temp1 - temp2);
                }
                else
                    result = Long.toString(firsNumber.longValue() - secondNumber.longValue());
                break;
            case "*":
                if (isRealNumbers) {
                    double temp1 = firsNumber.doubleValue();
                    double temp2 = secondNumber.doubleValue();
                    result = Double.toString(temp1 * temp2);
                }
                else
                    result = Long.toString(firsNumber.longValue() * secondNumber.longValue());
                break;
            case "/":
                result = Double.toString(firsNumber.doubleValue() / secondNumber.doubleValue());
                break;
        }
        historyCalculate.add(new NodeHistory(result,containerParts[0].partMathEx ,containerParts[1].partMathEx , sing));
        return result;
    }

    private class ContainerPart {
        String partMathEx;
        int index;

        public ContainerPart() {
            partMathEx = new String();
            index = -1;
        }

        public void setPart(String part, int index) {
            partMathEx = part;
            this.index = index;
        }

        public String getPartMathEx() {
            return partMathEx;
        }

        public int getIndex() {
            return index;
        }

        public void clear() {
            partMathEx = new String();
            index = -1;
        }
    }

}
