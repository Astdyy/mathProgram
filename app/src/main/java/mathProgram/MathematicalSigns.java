package mathProgram;

public final class MathematicalSigns {
    private static final char[] mathematicalSigns = new char[]{'+','-','/','*'};
    private  static final char[] numberTypesSystemsSigns = new char[]{'.','a','b', 'c','d','e', 'f','x'};

    private MathematicalSigns() {
    }
    public static boolean isMathSigns(char symbol){
        for (int i = 0; i < mathematicalSigns.length; i++) {
            if (mathematicalSigns[i] == symbol)
            {
                return true;
            }
        }
        return  false;
    }
    public static boolean isMathSigns(String str){
        if (str.length() == 1) {
            char symbol = str.charAt(0);
            for (int i = 0; i < mathematicalSigns.length; i++) {
                if (mathematicalSigns[i] == symbol)
                {
                    return true;
                }
            }
        }
        return  false;
    }
    public  static  boolean isTypeSystemsSigns(char character){
        for (char sings: numberTypesSystemsSigns) {
            if (sings == character)
            {
                return true;
            }
        }
        return false;
    }
    public  static  boolean isContainTypeSystemSigns(String str){
        for (int i = 0; i < str.length(); i++) {
                if (isTypeSystemsSigns(str.charAt(i)))
                {
                    return true;
                }
        }
        return false;
    }
    public static String replaceСomma(String mathExp){
        StringBuilder stringBuilder = new StringBuilder(mathExp);
        if (stringBuilder.indexOf(",") > 0)
        {
            while (stringBuilder.indexOf(",")> 0)
            {
                stringBuilder.setCharAt(stringBuilder.indexOf(","),'.');
            }
            return stringBuilder.toString();
        }else
        {
            return  mathExp;
        }
    }

}
