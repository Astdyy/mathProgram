package mathProgram;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Programm {
    public static void main(String[] args) throws Exception {
        NumberCounter numberCounter = new NumberCounter();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a mathematical expression");
        String mathEx =  sc.nextLine();
        int countUnknownValues= numberCounter.countUnknownValues(mathEx);
        System.out.println("Count Unknown Values: "+ countUnknownValues);
        List<Double> unknownValuesList = new ArrayList<>();
        for (int i = 0; i < countUnknownValues; i++) {
            System.out.println("Enter unknown value number " + (i+ 1));
            unknownValuesList.add(Double.parseDouble(sc.nextLine()));
        }
        String result =numberCounter.countNumbers(unknownValuesList);
        System.out.println("Result: " + result);
        numberCounter.getHistory();
    }
}
