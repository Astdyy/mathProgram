package mathProgram;

public class MathematicalExpressionError extends Exception {
    public MathematicalExpressionError(String message) {
        super(message);
    }
}
