package mathProgram;

import java.util.ArrayList;
import java.util.List;

public class MathematicalExpression {
    private String mathExpression;
    private int countParenthesis;
    private boolean isConvertToDecimal;

    public MathematicalExpression(String mathExp) {
        setMathExpression(mathExp);
        isConvertToDecimal = false;
    }

    public MathematicalExpression(String mathExp, boolean convertToDecimal) {
        setMathExpression(mathExp);
        isConvertToDecimal = isConvertToDecimal;
    }

    public void setMathExpression(String mathExp) {
        char[] chars = MathematicalSigns.replaceСomma(mathExp).toCharArray();
        State previousState = State.SEARCH;
        State state = State.SEARCH;
        int openParenthesis = 0;
        int closeParenthesis = 0;
        StringBuilder mathExpBuilder = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(') {
                openParenthesis++;
                mathExpBuilder.append(chars[i]);
                continue;
            } else if (chars[i] == ')') {
                closeParenthesis++;
                mathExpBuilder.append(chars[i]);
                if (openParenthesis < closeParenthesis) {
                    mathExpression = null;
                    return;
                }
                continue;
            }
            switch (state) {
                case SEARCH:
                    if (Character.isDigit(chars[i]) || MathematicalSigns.isTypeSystemsSigns(chars[i])) {
                        if (previousState == State.DIGIT_RECORDER) {
                            state = State.ERROR;
                            continue;
                        }
                        mathExpBuilder.append(chars[i]);
                        previousState = state;
                        state = State.DIGIT_RECORDER;
                    } else if (MathematicalSigns.isMathSigns(chars[i])) {
                        if (previousState == State.MATHEMATICAL_SIGN_RECORDER) {
                            if (chars[i] == '-') {
                                mathExpBuilder.append(chars[i]);
                            } else {
                                state = State.ERROR;
                            }
                        } else {
                            mathExpBuilder.append(chars[i]);
                            previousState = state;
                            state = State.MATHEMATICAL_SIGN_RECORDER;
                        }
                    } else if (Character.isAlphabetic(chars[i])) {
                        state = State.ERROR;
                    }
                    break;
                case DIGIT_RECORDER:
                    if (Character.isDigit(chars[i]) || MathematicalSigns.isTypeSystemsSigns(chars[i])) {
                        mathExpBuilder.append(chars[i]);
                    } else if (MathematicalSigns.isMathSigns(chars[i])) {
                        mathExpBuilder.append(chars[i]);
                        previousState = state;
                        state = State.MATHEMATICAL_SIGN_RECORDER;
                    } else if (Character.isAlphabetic(chars[i])) {
                        state = State.ERROR;
                    } else if (chars[i] == ' ') {
                        previousState = state;
                        state = State.SEARCH;
                    }
                    break;
                case MATHEMATICAL_SIGN_RECORDER:
                    if (Character.isDigit(chars[i]) || MathematicalSigns.isTypeSystemsSigns(chars[i])) {
                        mathExpBuilder.append(chars[i]);
                        previousState = state;
                        state = State.DIGIT_RECORDER;
                    } else if (MathematicalSigns.isMathSigns(chars[i])) {
                        if (chars[i] == '-') {
                            mathExpBuilder.append(chars[i]);
                        } else {
                            state = State.ERROR;
                        }
                    } else if (Character.isAlphabetic(chars[i])) {
                        state = State.ERROR;
                    } else if (chars[i] == ' ') {
                        previousState = state;
                        state = State.SEARCH;
                    }
                    break;
                case ERROR:
                    mathExpression = null;
                    return;
            }
        }
        if (closeParenthesis != openParenthesis) {
            mathExpression = null;
        } else {
            countParenthesis = openParenthesis * 2;
            mathExpression = mathExpBuilder.toString();
        }
    }

    public List<String> toList() {
        List<String> result = new ArrayList<>();
        StringBuilder partBuilder = new StringBuilder();
        State previousState = State.MATHEMATICAL_SIGN_RECORDER;
        State state = State.MATHEMATICAL_SIGN_RECORDER;
        if (mathExpression.length() > 0) {
            char[] characters = mathExpression.toCharArray();
            for (char symbol : characters) {
                switch (state) {
                    case DIGIT_RECORDER:
                        if (Character.isDigit(symbol) || MathematicalSigns.isTypeSystemsSigns(symbol)) {
                            partBuilder.append(symbol);
                        } else if (MathematicalSigns.isMathSigns(symbol)) {
                            if (partBuilder.length() > 0) {
                                result.add(partBuilder.toString());
                                partBuilder = new StringBuilder();
                            }

                            result.add(Character.toString(symbol));
                            previousState = state;
                            state = State.MATHEMATICAL_SIGN_RECORDER;

                        } else if (symbol == '(' | symbol == ')') {
                            if (partBuilder.length() > 0) {
                                result.add(partBuilder.toString());
                                partBuilder = new StringBuilder();
                            }
                            result.add(Character.toString(symbol));
                        }
                        break;
                    case MATHEMATICAL_SIGN_RECORDER:
                        if (Character.isDigit(symbol) || MathematicalSigns.isTypeSystemsSigns(symbol)) {
                            partBuilder.append(symbol);
                            state = State.DIGIT_RECORDER;
                        } else if (MathematicalSigns.isMathSigns(symbol)) {
                            if (symbol == '-') {
                                partBuilder.append(symbol);
                                previousState = state;
                                state = State.DIGIT_RECORDER;
                            } else {
                                if (partBuilder.length() > 0) {
                                    result.add(partBuilder.toString());
                                    partBuilder = new StringBuilder();
                                }
                                result.add(Character.toString(symbol));
                            }
                        } else if (symbol == '(' | symbol == ')') {
                            if (partBuilder.length() > 0) {
                                result.add(partBuilder.toString());
                                partBuilder = new StringBuilder();
                            }
                            result.add(Character.toString(symbol));
                        }
                        break;
                }
            }
        }
        if (partBuilder.length() > 0) {
            result.add(partBuilder.toString());
        }
        return result;
    }

    public List<String> convertToDecimalSystem() throws Exception{
        if (mathExpression.length() != 0) {
            List<String> result = toList();
            for (int i = 0; i < result.size(); i++) {
                String temp = result.get(i);
                if (MathematicalSigns.isContainTypeSystemSigns(result.get(i))) {
                    if (temp.indexOf(".") > 0) {
                        if (temp.indexOf(".") == 0 | temp.indexOf(".") == temp.length() - 1) {
                            throw new MathematicalExpressionError("Error realNumbers ");
                        }
                    } else if (temp.charAt(1) == 'x') {
                        result.set(i, Long.toString(Long.parseLong(deleteNumberSystem(temp), 16)));
                    } else if (temp.charAt(1) == 'b') {
                        result.set(i, Long.toString(Long.parseLong(deleteNumberSystem(temp), 2)));
                    } else {
                        throw new MathematicalExpressionError("Error convertToDecimalSystem");
                    }
                } else if (temp.charAt(0) == '0' & !temp.contains("8") & !temp.contains("9")) {
                    result.set(i, Long.toString(Long.parseLong(temp, 8)));
                }
            }
            return result;
        } else {
            return null;
        }
    }

    private String deleteNumberSystem(String str)
    {
        StringBuilder replaceNum = new StringBuilder();
        for (int j = 2; j < str.length(); j++) {
            replaceNum.append(str.charAt(j));
        }
        return replaceNum.toString();
    }
    public int getCountParenthesis() {
        return countParenthesis;
    }

    public String getMathExpression() {
        return mathExpression;
    }

}
