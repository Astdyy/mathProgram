package mathProgram;

public class NodeHistory {
    private String result;
    private String firstValue;
    private String secondValue;
    private  String sing;

    public NodeHistory(String result, String firstValue, String secondValue, String sing) {
        this.result = result;
        this.firstValue = firstValue;
        this.secondValue = secondValue;
        this.sing = sing;
    }

    public String getResult() {
        return result;
    }

    public String getFirstValue() {
        return firstValue;
    }

    public String getSecondValue() {
        return secondValue;
    }

    public String getSing() {
        return sing;
    }

    @Override
    public String toString() {
        return firstValue + sing + secondValue + "=" + result;
    }
    public  boolean contains(String strVal )
    {
        if (strVal == firstValue | strVal ==secondValue)
        {
            return true;
        }
        return false;
    }
}
