package mathProgram;


import java.util.List;

public class Visualization {
    public static void visualize(List<NodeHistory> history, MathematicalExpression expression){
        for (int i = history.size() -1; i >= 0 ; i--) {
            System.out.println(history.get(i).toString());
            if (i != 0)
            {
                System.out.println("|");
            }
        }
    }
}

