package mathProgram;

import java.util.ArrayList;
import java.util.List;

public class UnknownValuesParser {
    private String input;
    private List<String> nameUnknownValues;

    public UnknownValuesParser() {
        nameUnknownValues = new ArrayList<>();
    }
    public int fiendUnknownValues(String str){
        nameUnknownValues.clear();
        input = str;
        char[] chars = str.toCharArray();
        UnknownValuesState previousState = UnknownValuesState.SEARCH;
        UnknownValuesState state = UnknownValuesState.SEARCH;
        StringBuilder nameBuilder = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            switch (state){
                case SEARCH:
                    if (Character.isAlphabetic(chars[i])){
                        nameBuilder.append(chars[i]);
                        previousState =state;
                        state = UnknownValuesState.UNKNOWN_VALUES_RECORDER;
                    }else if(MathematicalSigns.isMathSigns(chars[i])){
                        previousState =state;
                        state = UnknownValuesState.MATHEMATICAL_SIGN;
                    }
                    break;
                case UNKNOWN_VALUES_RECORDER:
                    if (Character.isAlphabetic(chars[i]) || Character.isDigit(chars[i])){
                        nameBuilder.append(chars[i]);
                    }else if (chars[i] == ' '){
                        if (!nameUnknownValues.contains(nameBuilder.toString()))
                        {
                            nameUnknownValues.add(nameBuilder.toString());
                        }
                        nameBuilder = new StringBuilder();
                        previousState = state;
                        state= UnknownValuesState.SEARCH;
                    }else if(MathematicalSigns.isMathSigns(chars[i])){
                        if (!nameUnknownValues.contains(nameBuilder.toString()))
                        {
                            nameUnknownValues.add(nameBuilder.toString());
                        }
                        nameBuilder = new StringBuilder();
                        previousState = state;
                        state = UnknownValuesState.MATHEMATICAL_SIGN;
                    }
                    break;
                case MATHEMATICAL_SIGN:
                    if (Character.isAlphabetic(chars[i])){
                        nameBuilder.append(chars[i]);
                        previousState = state;
                        state = UnknownValuesState.UNKNOWN_VALUES_RECORDER;
                    }else if (chars[i] == ' '){
                        previousState = state;
                        state= UnknownValuesState.SEARCH;
                    }else if(MathematicalSigns.isMathSigns(chars[i]) & chars[i] != '-'){
                        state = UnknownValuesState.ERROR;
                    }
                    break;
                case ERROR:
                    return -1;
            }
        }
        if (nameBuilder.length() != 0 & !nameUnknownValues.contains(nameBuilder.toString()))
        {
            nameUnknownValues.add(nameBuilder.toString());
        }
        return nameUnknownValues.size();
    }
    public MathematicalExpression addUnknownValues(List<Double> numbersByUnknownValues ){
        if (nameUnknownValues.size() == numbersByUnknownValues.size())
        {for (int i = 0; i < nameUnknownValues.size(); i++) {
               while (input.contains(nameUnknownValues.get(i))) {
                    input = input.replace(nameUnknownValues.get(i), numbersByUnknownValues.get(i).toString());
                }
            }
        }else {
            throw new IllegalArgumentException();
        }
        return new MathematicalExpression(input);
    }
    public enum UnknownValuesState {
        SEARCH(0), UNKNOWN_VALUES_RECORDER(1),ERROR(-1), MATHEMATICAL_SIGN(2);
        private final int code;
        private  UnknownValuesState(int code)
        {
            this.code = code;
        }
        public int getCode() {
            return code;
        }
    }


}
