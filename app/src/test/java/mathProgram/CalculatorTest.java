package mathProgram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class CalculatorTest {

    @Test
    void test() throws Exception {
        Calculator cl = new Calculator();
        Assertions.assertEquals("111",cl.сalculate(new MathematicalExpression("5+4*9/2-3+10*8+11")));
        Assertions.assertEquals("1000",cl.сalculate(new MathematicalExpression("100/2*10+500")));
        Assertions.assertEquals("100",cl.сalculate(new MathematicalExpression("50+10*10-50")));
        Assertions.assertEquals("-219",cl.сalculate(new MathematicalExpression("4*5+(8+9-23*10)-26")));
        Assertions.assertEquals("-90",cl.сalculate(new MathematicalExpression("-200+100+10")));
        Assertions.assertEquals("-40",cl.сalculate(new MathematicalExpression("10-20-30+100-100")));
        Assertions.assertEquals("6",cl.сalculate(new MathematicalExpression("5-3+4")));
        Assertions.assertEquals("19",cl.сalculate(new MathematicalExpression("5+4-3+2+10-3+4")));
        Assertions.assertEquals("12",cl.сalculate(new MathematicalExpression("3*2*4/2")));
        Assertions.assertEquals("14",cl.сalculate(new MathematicalExpression("3+4*5+(8+9)-26")));
    }

    @Test
    void realNumbersTest() throws Exception {
        Calculator cl = new Calculator();
        Assertions.assertEquals("-194.79999999999998",cl.сalculate(new MathematicalExpression("4*5+(8.8+9-23*10)-2.6")));
    }
    @Test
    void numberSystemsTest() throws Exception {
        Calculator cl = new Calculator();
        Assertions.assertEquals("1500",cl.сalculate(new MathematicalExpression("  0b111110100 +  0x1f4 +   0764 ")));
        Assertions.assertEquals("1512.4",cl.сalculate(new MathematicalExpression("  0b100 * 0b101  +  ( 0x320 + 0x3b1 - 23*10) - 22.6")) );
    }
    @Test
    void errorTest(){
        Calculator cl = new Calculator();
        Assertions.assertThrows(MathematicalExpressionError.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                cl.сalculate(new MathematicalExpression(".100/2*10+500"));
            }
        });
        Assertions.assertThrows(MathematicalExpressionError.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                cl.сalculate(new MathematicalExpression("10..0/2*10+500"));
            }
        });
    }

    @Test
    void visualTest() throws Exception {
        Calculator cl = new Calculator();
        MathematicalExpression me = new MathematicalExpression("5+4*9/2-3+10*8+11");
        Assertions.assertEquals("111",cl.сalculate(me));
        Visualization.visualize(cl.getHistoryCalculate(),me);
    }
}