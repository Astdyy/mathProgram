package mathProgram;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

public class FullTest {
    @Test
    void test1() throws Exception {
        String inp = "(x1+x2 + 27)-(x3*x5/(-x1 + x6))*myComplexVar1";
        Double[] unknownValues = new Double[]{10D, 20D,2D,20D,20D,2D};
        NumberCounter numberCounter = new NumberCounter();
        numberCounter.countUnknownValues(inp);
        String result = numberCounter.countNumbers(Arrays.asList(unknownValues));
        Assertions.assertEquals("49", result);
    }
}
