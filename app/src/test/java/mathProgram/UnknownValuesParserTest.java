package mathProgram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.List;

class UnknownValuesParserTest {
    UnknownValuesParser uvp = new UnknownValuesParser();
    @Test
    void testFiendUnknownValues(){

        int countUV = uvp.fiendUnknownValues(" 2 + asd + 32");
        Assertions.assertEquals(1 , countUV);
        Assertions.assertEquals(2, uvp.fiendUnknownValues("22 + x1 * (45 + x2)"));
        Assertions.assertEquals(4 , uvp.fiendUnknownValues("   x1   + y2    +qwe12   / 34   + val23 "));
        Assertions.assertEquals(4 , uvp.fiendUnknownValues("   x1   *(( y2    +qwe12 ))  / (34   + val23) / 554 "));
        Assertions.assertEquals(0 , uvp.fiendUnknownValues(" 32 + 234 + 33333"));
    }
    @Test
    void ErrorTestFiendUnknownValues(){
        Assertions.assertEquals(-1 , uvp.fiendUnknownValues("  123 ++ xasd + 43"));
        Assertions.assertEquals(-1 , uvp.fiendUnknownValues(" 23+/ qwe12 + 45  "));
        Assertions.assertThrows(IllegalArgumentException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                uvp.fiendUnknownValues("   23 +   asd   + 43");
                List<Double> inp = new ArrayList<>();
                inp.add(23D);
                inp.add(23D);
                MathematicalExpression me = uvp.addUnknownValues(inp);
            }
        });
    }
    @Test
    void TestAddUnknownValues(){
        uvp.fiendUnknownValues("   23 +   asd   + 43");
        List<Double> inp = new ArrayList<>();
        inp.add(23D);
        MathematicalExpression me = uvp.addUnknownValues(inp);
        Assertions.assertEquals("23+23.0+43" , me.getMathExpression());
        uvp.fiendUnknownValues("   23 +   asd   + 43 + asd");
        List<Double> inp2 = new ArrayList<>();
        inp2.add(244D);
        MathematicalExpression me1 = uvp.addUnknownValues(inp2);
        Assertions.assertEquals("23+244.0+43+244.0" , me1.getMathExpression());
    }
}