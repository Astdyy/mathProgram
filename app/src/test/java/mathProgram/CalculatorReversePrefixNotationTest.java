package mathProgram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculatorReversePrefixNotationTest {
    @Test
    void SimpleParseText() throws Exception {
        Calculator cl = new Calculator();
        Assertions.assertEquals("-200 100 + 10 +",cl.parseToReversePrefixNotation(new MathematicalExpression("-200+100+10")));
        Assertions.assertEquals("5 3 - 4 +",cl.parseToReversePrefixNotation(new MathematicalExpression("5-3+4")));
        Assertions.assertEquals("5 9 + 3 - 4 +",cl.parseToReversePrefixNotation(new MathematicalExpression("5+9-3+4")));
        Assertions.assertEquals("5 19 + 3 - 42 +",cl.parseToReversePrefixNotation(new MathematicalExpression("5+19-3+42")));
        Assertions.assertEquals("5 4 + 3 - 2 + 10 + 3 - 4 + 35 -",cl.parseToReversePrefixNotation(new MathematicalExpression("5+4-3+2+10-3+4-35")));
        Assertions.assertEquals("2432 34 + 999999 - 10000000000 + 4541223 -",cl.parseToReversePrefixNotation(new MathematicalExpression("2432+34-999999+10000000000- 4541223")));
    }
    @Test
    void SimpleParseText2() throws Exception {//multiplication and division
        Calculator cl = new Calculator();
        Assertions.assertEquals("3 2 * 4 * 2 /",cl.parseToReversePrefixNotation(new MathematicalExpression("3*2*4/2")));
        Assertions.assertEquals("5 4 9 * +",cl.parseToReversePrefixNotation(new MathematicalExpression("5+4*9")));
        Assertions.assertEquals("5 9 5 * 2 / +",cl.parseToReversePrefixNotation(new MathematicalExpression("5+9*5/2")));
        Assertions.assertEquals("5 4 9 * 2 / + 3 - 10 8 * + 11 +",cl.parseToReversePrefixNotation(new MathematicalExpression("5+4*9/2-3+10*8+11")));
    }
    @Test
    void parenthesisTest() throws Exception {
        Calculator cl = new Calculator();
        Assertions.assertEquals("3 4 5 * + 8 9 + + 26 -",cl.parseToReversePrefixNotation(new MathematicalExpression("3+4*5+(8+9)-26")));
        Assertions.assertEquals("4 5 * 8 9 + 23 10 * - + 26 -",cl.parseToReversePrefixNotation(new MathematicalExpression("4*5+(8+9-23*10)-26")));
        Assertions.assertEquals("4 5 * 8 9 + 23 10 * - 43 - + 26 -",cl.parseToReversePrefixNotation(new MathematicalExpression("4*5+((8+9-23*10)-43)-26")));
        Assertions.assertEquals("10 20 + 27 + 2 20 * 10 20 + / 2 * -",cl.parseToReversePrefixNotation(new MathematicalExpression("(10+20+27)-(2*20/(10+20))*2")));
        Assertions.assertEquals("4 5 * 8 9 + 23 10 * - + 26 -",cl.parseToReversePrefixNotation(new MathematicalExpression("4*5+(((8+9))-23*10)-26")));
        Assertions.assertEquals("4 5 * 8 9 + 23 10 * - + 26 -", cl.parseToReversePrefixNotation(new MathematicalExpression("4*5+(8+9-23*10)-26")));
    }
    @Test
    void numberSystemsTest() throws Exception {
        Calculator cl = new Calculator();
        Assertions.assertEquals("3 4 5 * + 8 9 + + 26 -",cl.parseToReversePrefixNotation(new MathematicalExpression("3+4*5+(8+9)-26")));
       // Assertions.assertEquals();
    }
    @Test
    void errorTest(){

    }
}