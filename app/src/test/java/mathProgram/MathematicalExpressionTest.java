package mathProgram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


class MathematicalExpressionTest {


    @Test
    void simpleTest() {
        MathematicalExpression me1 = new MathematicalExpression(" 4  +  5-3 ");
        Assertions.assertEquals("4+5-3", me1.getMathExpression());
        me1.setMathExpression(" 55     +   34    /25    ");
        Assertions.assertEquals("55+34/25", me1.getMathExpression());
    }

    @Test
    void parenthesisTest() {
        MathematicalExpression me1 = new MathematicalExpression(" 4  + (  5-3 )* 100 ");
        Assertions.assertEquals("4+(5-3)*100", me1.getMathExpression());
        me1.setMathExpression(" 55     +  ( 34  -234)   /25    ");
        Assertions.assertEquals("55+(34-234)/25", me1.getMathExpression());
        me1.setMathExpression("(  (  10 + 34   )   * 42) / 2   ");
        Assertions.assertEquals("((10+34)*42)/2", me1.getMathExpression());
        me1.setMathExpression("((  (  (  (  10 + 34   )   * 42))  )  ) / 2   ");
        Assertions.assertEquals("(((((10+34)*42))))/2", me1.getMathExpression());
        me1.setMathExpression("  4*  5+  (8  +  9  -23*   10  )   -  26");
        Assertions.assertEquals("4*5+(8+9-23*10)-26", me1.getMathExpression());
    }

    @Test
    void errorTest() {
        MathematicalExpression me1 = new MathematicalExpression("1 24 +23");
        Assertions.assertNull(me1.getMathExpression());
        me1.setMathExpression("(( 2 + 3)");
        Assertions.assertNull(me1.getMathExpression());
        me1.setMathExpression(" 23 ++ 56");
        Assertions.assertNull(me1.getMathExpression());

    }

    @Test
    void trueExTest() {
        MathematicalExpression me1 = new MathematicalExpression("23+33-333/2+4");
        Assertions.assertEquals("23+33-333/2+4", me1.getMathExpression());
        me1.setMathExpression("23+33-(333/2+4)");
        Assertions.assertEquals("23+33-(333/2+4)", me1.getMathExpression());
    }

    @Test
    void negativeTest() {
        MathematicalExpression me1 = new MathematicalExpression("-30  +40  ");
        Assertions.assertEquals("-30+40", me1.getMathExpression());
        me1.setMathExpression(" 345 +  - 33");
        Assertions.assertEquals("345+-33", me1.getMathExpression());
        me1.setMathExpression(" 345 +  - 33    / -54   ");
        Assertions.assertEquals("345+-33/-54", me1.getMathExpression());
    }

    @Test
    void realNumbersTest() {

        MathematicalExpression me1 = new MathematicalExpression(" 5.5  + 40  ");
        Assertions.assertEquals("5.5+40", me1.getMathExpression());
        me1.setMathExpression("  4*  5.5 +  (8  +  9.48  -23*   10  )   -  26000");
        Assertions.assertEquals("4*5.5+(8+9.48-23*10)-26000", me1.getMathExpression());
        me1.setMathExpression("  4.5 + 38 - 3333.3    ");
        Assertions.assertEquals("4.5+38-3333.3", me1.getMathExpression());
        me1.setMathExpression("  4,5 +   38 - 3333,3    ");
        Assertions.assertEquals("4.5+38-3333.3", me1.getMathExpression());
    }

    @Test
    void toListTest() {
        MathematicalExpression me1 = new MathematicalExpression(" 23   +   -33   -333  /  2  +   4  ");
        boolean arrayEquals = isArrayEquals(new String[]{"23", "+", "-33", "-", "333", "/", "2", "+", "4"}, me1.toList().toArray(String[]::new));
        Assertions.assertTrue(arrayEquals);
        me1.setMathExpression("  4*  5+  (8  +  9  -23*   10  )   -  26");
        boolean arrayEquals2 = isArrayEquals(new String[]{"4", "*", "5", "+", "(", "8", "+", "9", "-", "23", "*", "10", ")", "-", "26"}, me1.toList().toArray(String[]::new));
        Assertions.assertTrue(arrayEquals2);
        me1.setMathExpression("  -200    + 100+  10   ");
        boolean arrayEquals3 = isArrayEquals(new String[]{"-200", "+", "100", "+", "100"}, me1.toList().toArray(String[]::new));
    }
    @Test
    void convertToDecimalSystemTest() throws Exception
    {
       MathematicalExpression me1 = new MathematicalExpression("  234   + 100+  10 * 55.5  + 34.2  / 2");
        List<String> res = me1.convertToDecimalSystem();
        boolean arrayEquals4 = isArrayEquals(new String[]{"234", "+", "100", "+", "10","*","55.5","+","34.2","/","2"}, me1.toList().toArray(String[]::new));
        Assertions.assertTrue(arrayEquals4);
        MathematicalExpression me2 = new MathematicalExpression("  0b111110100 +  0x1f4 +   0764 ");
        List<String> res2 = me2.convertToDecimalSystem();
        boolean arrayEquals5 = isArrayEquals(new String[]{"500", "+", "500", "+", "500"}, me2.toList().toArray(String[]::new));
        Assertions.assertTrue(arrayEquals5);
        MathematicalExpression me3 = new MathematicalExpression("  0b111110100 +  0x1f4 +   0764 ");
        List<String> res3 = me2.convertToDecimalSystem();
        boolean arrayEquals3 = isArrayEquals(new String[]{"500", "+", "500", "+", "500"}, me2.toList().toArray(String[]::new));
        Assertions.assertTrue(arrayEquals3);
    }

    public boolean isArrayEquals(String[] arr1, String[] arr2) {
        boolean arrayEquals = true;
        if (arr1.length == arr2.length) {
            for (int i = 0; i < arr1.length; i++) {
                if (arr1[i] == arr2[i]) {
                    arrayEquals = false;
                }
            }
        } else {
            arrayEquals = false;
        }
        return arrayEquals;
    }
}